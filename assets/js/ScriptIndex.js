const SwitchBody = () => {
  document.getElementById("bodyBanner").style.transform = "translateY(0)";
  document.getElementById("bodyBanner").style.transition = "1s";
};
setTimeout(SwitchBody, 400);

const SwitchMask = () => {
  document.getElementById("divBannerMask").style.transform = "translateY(0)";
  document.getElementById("divBannerMask").style.transition = "1s";
};
setTimeout(SwitchMask, 600);

const SwitchDivBienvenue = () => {
  document.getElementById("divBienvenue").style.transform = "translateY(0)";
  document.getElementById("divBienvenue").style.transition = "1.5s";
};
setTimeout(SwitchDivBienvenue, 800);

const SwitchDivLimite = () => {
  document.getElementById("divDateLimite").style.transform = "translateY(0)";
  document.getElementById("divDateLimite").style.transition = "1.5s";
  document.body.style.overflowY = "scroll";
};
setTimeout(SwitchDivLimite, 1500);