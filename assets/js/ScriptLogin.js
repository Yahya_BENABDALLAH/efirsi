// Form Change
$("input[name=radioOptions]:radio").change(function (e) {
  let value = e.target.value.trim();
  $('[class^="form-secondaire"]').css("display", "none");

  switch (value) {
    case "Boursier":
      $(".form-secondaire-Boursier").fadeIn(1000);
      break;
    case "Candidat":
      $(".form-secondaire-Candidat").fadeIn(1000);
      break;
    default:
      break;
  }
});
// 
